<?php

use App\Admin\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuizzesController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\ResultsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/quizzes', QuizzesController::class);
Route::resource('/questions', QuestionsController::class);
Route::resource('/results', ResultsController::class);
Route::get('/questions/{id}/create', [App\Http\Controllers\QuestionsController::class, 'create'])->name('create');
Route::get('/quizzes/{id}/start', [App\Http\Controllers\QuizzesController::class, 'start'])->name('start');
Route::post('/quizzes/result', [App\Http\Controllers\QuizzesController::class, 'result'])->name('result');