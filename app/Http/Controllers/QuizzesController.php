<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Models\Quiz;
use App\Models\Result;

class QuizzesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q=Quiz::all();
        
        return view('quizzes.index', [
            'quiz'=>$q
        ]);
    }


    public function start($id)
    {
        $quiz = Quiz::findOrFail($id);
        $questions = Question::all()->where('quiz_id', $id);

        
        return view('/quizzes.start', [
            'quiz'=> $quiz,
            'questions' => $questions
        ]);
    }


    public function result(Request $request)
    {
        $data=$request->except('_token', 'quiz_id');

        $num_questions=(count($data)-1)/2;
        $num_correct=0;
        $quiz_id= $request->input('quiz_id');
        $mail=$data['email'];

        for($x=0; $x<$num_questions; $x++)
        {
            if($data["correct".$x]==$data["option".$x])
            {
                $num_correct++;
            }
        }
                
        $result=Result::make([
                'email' => $mail,
                'quiz_id' => $quiz_id,
                'num_questions' => $num_questions,
                'num_corr_answ' => $num_correct
                
            ]);
            $result->save();


        return redirect ('/results');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('quizzes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quiz=Quiz::create([
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return redirect ('/quizzes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quiz = Quiz::findOrFail($id);
        $questions = Question::all()->where('quiz_id', $id);

        
        return view('/quizzes.show', [
            'quiz'=> $quiz,
            'questions' => $questions
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz = Quiz::findOrFail($id);
        
        return view('quizzes.edit')->with('quiz', $quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quiz=Quiz::where('id', $id)
        ->update([
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return redirect ('/quizzes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = Quiz::findOrFail($id);

        $quiz->delete();

        return redirect('/quizzes');
    }

}
