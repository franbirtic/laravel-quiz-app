<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;


class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q=Question::all();
        
        return view('questions.index', [
            'questions'=>$q
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view ('questions.create')->with('id', $id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $q=Question::make([
            'question' => $request->input('question'),
            'optionA' => $request->input('optionA'),
            'optionB' => $request->input('optionB'),
            'optionC' => $request->input('optionC'),
            'optionD' => $request->input('optionD'),
            'correct' => $request->input('correct'),
            
        ]);
        $q->quiz_id = $request->input('quiz_id');
        $q->save();
        return redirect ("/quizzes/".$q->quiz_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);
        
        return view('questions.edit')->with('question', $question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question=Question::where('id', $id)
        ->update([
            'question' => $request->input('question'),
            'optionA' => $request->input('optionA'),
            'optionB' => $request->input('optionB'),
            'optionC' => $request->input('optionC'),
            'optionD' => $request->input('optionD'),
            'correct' => $request->input('correct'),
        ]);

        return redirect ("/quizzes");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);

        $question->delete();

        return redirect('/quizzes');
    }
}
