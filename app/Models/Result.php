<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;
    
    protected $fillable = ['email', 'num_questions', 'num_corr_answ', 'quiz_id'];

    protected $table = 'results';

    protected $primaryKey = 'id';

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
}
