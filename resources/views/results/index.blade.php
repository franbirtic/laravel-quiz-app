@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   @foreach ($results as $item)
                        <div class="justify-content-center alert alert-success">

                                <h4>User email : {{ $item->email }}</h4>
                                <br><h4>Correct answers : {{ $item->num_corr_answ }}</h4>
                                <br><h4>Total questions : {{ $item->num_questions }}</h4>
                                <br><h4>Total questions : {{ $item->quiz->title }}</h4>
                            
                        </div>
                    <hr>
                   @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
