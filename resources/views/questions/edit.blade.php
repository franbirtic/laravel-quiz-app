@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <h4 class="p-3">Update quiz</h4>

    <form action="/questions/{{ $question->id}}" method="post">
        <div class="form-group">
            @csrf
            @method('PUT')
            <input type="text" class="form-control" id="question" name ="question" value="{{$question->question}}">
    
            <input type="text" class="form-control" id="optionA" name = "optionA"  value="{{$question->optionA}}">
    
            <input type="text" class="form-control" id="optionB" name = "optionB"  value="{{$question->optionB}}">
    
            <input type="text" class="form-control" id="optionC" name = "optionC"  value="{{$question->optionC}}">
    
            <input type="text" class="form-control" id="optionD" name = "optionD"  value="{{$question->optionD}}">
    
            <input type="text" class="form-control" id="correct" name = "correct"  value="{{$question->correct}}">
    
    
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection