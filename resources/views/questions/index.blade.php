@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href='/questions/create'><button>Create new Question</button></a>

                   @foreach ($questions as $item)
                   <div class="justify-content-center alert alert-success">
                        <a href='/questions/{{ $item->id }}/edit'><button class="float-right">edit this question</button></a>
                        <h4>Question : {{ $item->question }}</h4>
                        <br><h4>option A : {{ $item->optionA }}</h4>
                        <br><h4>option B : {{ $item->optionB }}</h4>
                        <br><h4>option C : {{ $item->optionC }}</h4>
                        <br><h4>option D : {{ $item->optionD }}</h4>
                        <br><h4>correct : {{ $item->correct }}</h4>
                        
                    </div>
                    <hr>
                   @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
