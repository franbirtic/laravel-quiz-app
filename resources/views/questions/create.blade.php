@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <h4 class="p-3">Create new question</h4>


    <form action="{{url('/questions')}}" method="post">
        <div class="form-group">
            @csrf
            <input type="text" class="form-control" id="question" name ="question" placeholder="Enter question">

            <input type="text" class="form-control" id="optionA" name = "optionA"  placeholder="Enter option A">

            <input type="text" class="form-control" id="optionB" name = "optionB"  placeholder="Enter option B">

            <input type="text" class="form-control" id="optionC" name = "optionC"  placeholder="Enter option C">

            <input type="text" class="form-control" id="optionD" name = "optionD"  placeholder="Enter option D">

            <input type="text" class="form-control" id="correct" name = "correct"  placeholder="Enter correct answer">

            <input type="hidden" id="quiz_id" name = "quiz_id" value="{{$id}}">

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
