@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Quiz detail</h4> <br>
                    Title : {{$quiz->title}} <br>
                    <small>Description : {{$quiz->description}}</small>
                </div>
                    
                <div>
                    <h4>Questions</h4>
                    <a href="{{url('/questions/'.$quiz->id.'/create/')}}"><button>Add new question</button></a>


                    @foreach ($questions as $item)
                    <div class="justify-content-center alert alert-success">
                         <a href='/questions/{{ $item->id }}/edit'><button class="float-right">edit this question</button></a>

                         <form action="/questions/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="float-right">
                                Delete
                            </button>
                        </form>
                         <h4>Question : {{ $item->question }}</h4>
                         <br><h4>option A : {{ $item->optionA }}</h4>
                         <br><h4>option B : {{ $item->optionB }}</h4>
                         <br><h4>option C : {{ $item->optionC }}</h4>
                         <br><h4>option D : {{ $item->optionD }}</h4>
                         <br><h4>correct : {{ $item->correct }}</h4>
                         
                     </div>
                     <hr>
                    @endforeach
                </div>


                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
