@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <h4 class="p-3">Update quiz</h4>

    <form action="/quizzes/{{ $quiz->id}}" method="post">
        <div class="form-group">
            @csrf
            @method('PUT')
            <input type="text" class="form-control" id="title" name ="title" placeholder="{{$quiz->title}}">

            <input type="text" class="form-control" id="description" name = "description"  placeholder="{{$quiz->description}}">

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
