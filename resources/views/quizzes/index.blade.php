@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @auth
                   <a href='/quizzes/create'><button class="btn btn-info">Create new Quiz</button></a>
                    <br><br>
                    @endauth
                   @foreach ($quiz as $item)
                        <div class="justify-content-center alert alert-success">

                            <a href='/quizzes/{{$item->id}}/start'><button class="float-right btn btn-primary m-1">Start Quiz</button></a>
                            @auth
                                <a href='/quizzes/{{ $item->id }}/edit'><button class="float-right btn btn-info m-1">Edit quiz info</button></a>

                                <form action="quizzes/{{$item->id}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="float-right btn btn-info m-1">
                                        Delete
                                    </button>
                                </form>

                                <a href="/quizzes/{{ $item->id }}"><button class="float-right btn btn-info m-1">Edit questions</button></a>
                            @endauth
                            <h4>Quiz title :<br> {{ $item->title }}</h4>
                            <br><h4>Quiz description : <br> {{ $item->description }}</h4>

                        </div>
                    <hr>
                   @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection