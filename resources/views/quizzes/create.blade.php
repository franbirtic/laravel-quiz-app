@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <h4 class="p-3">Create new quiz</h4>

    <form action="/quizzes" method="post">
        <div class="form-group">
            @csrf
            <input type="text" class="form-control" id="title" name ="title" placeholder="Enter title">

            <input type="text" class="form-control" id="description" name = "description"  placeholder="Enter description">

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
