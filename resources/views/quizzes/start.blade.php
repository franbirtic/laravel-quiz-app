@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Title : {{$quiz->title}}</h2> <br>
                    <h3>Description : {{$quiz->description}}</h3>
                </div>

                <div>
                   <b><h2>Questions</h2></b>
                  

                    <form action="/quizzes/result" method="post">
                        @csrf
                        <div class="card-header">
                            <input type="email" class="form-control" id="email" name ="email" placeholder="Enter E-mail">
                        </div>
                        @php($key=0)
                        @foreach ($questions as $item)
                            
                            <div class="justify-content-center alert alert-success">
                            <div class="form-group">
                                <div>
                                    <h2>{{$item->question}}</h2>
                                    
                                </div>
                                <div>
                                    <label for="optionA{{$item->id}}">{{$item->optionA}}</label>
                                    <input type="radio" id="optionA{{$item->id}}" name = "option{{$key}}"  value="{{$item->optionA}}">
                                </div>
                                <div>
                                    <label for="optionB{{$item->id}}">{{$item->optionB}}</label>
                                    <input type="radio" id="optionB{{$item->id}}" name = "option{{$key}}"  value="{{$item->optionB}}">
                                </div>
                                <div>
                                    <label for="optionC{{$item->id}}">{{$item->optionC}}</label>
                                    <input type="radio" id="optionC{{$item->id}}" name = "option{{$key}}"  value="{{$item->optionC}}">
                                </div>
                                <div>
                                    <label for="optionD{{$item->id}}">{{$item->optionD}}</label>
                                    <input type="radio" id="optionD{{$item->id}}" name = "option{{$key}}"  value="{{$item->optionD}}">
                                </div>
                                <input type="hidden" id="correct{{$item->id}}" name = "correct{{$key}}" value="{{$item->correct}}">
                                <hr>
                            </div>
                            </div>
                            @php($key++)
                        @endforeach
                    
                        <input type="hidden" id="quiz_id" name = "quiz_id" value="{{$quiz->id}}">
                
                        <button type="submit" class="btn btn-primary">Submit</button>
                        
                    </form>
                </div>
                <hr>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
