@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <h4 class="p-3">Enter your email to start Quiz</h4>

    <form action="/quizzes/{{$id}}/start" method="post">
        <div class="form-group">
            @csrf
            <input type="email" class="form-control" id="email" name ="email" placeholder="Enter E-mail">

            <button type="submit" class="btn btn-primary">Start Quiz</button>
        </div>
    </form>
</div>
@endsection
