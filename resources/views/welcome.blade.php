@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>
                        <h2>Welcome to the Quiz-question stuff app</h2>
                        <h3>As admin it is possible to edit various quizzes and question</h3>
                    </center>

            </div>
        </div>
    </div>
</div>
@endsection
